
Feature: Automation assignment for goibibo website

Scenario Outline: One way trip booking

Given user is on Goibibo website
When user click on one way trip button
And user select "<from>" and "<destination>" city
And user select depature_date
And user select <adult> <children> <infants>
And user select "<travel_class>"
And user click on search button
And click on price ascending
And click on book now
Then verify details of ticket

Examples:
|from|destination|adult|children|infants|travel_class|
|Pune|Delhi|2|1|1|Economy|


