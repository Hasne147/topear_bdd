$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/java/feature/Assignment_2_feature.feature");
formatter.feature({
  "line": 2,
  "name": "Automation assignment for goibibo website",
  "description": "",
  "id": "automation-assignment-for-goibibo-website",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 4,
  "name": "One way trip booking",
  "description": "",
  "id": "automation-assignment-for-goibibo-website;one-way-trip-booking",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 6,
  "name": "user is on Goibibo website",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "user click on one way trip button",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "user select \"\u003cfrom\u003e\" and \"\u003cdestination\u003e\" city",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "user select depature_date",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "user select \u003cadult\u003e \u003cchildren\u003e \u003cinfants\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "user select \"\u003ctravel_class\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "user click on search button",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "click on price ascending",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "click on book now",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "verify details of ticket",
  "keyword": "Then "
});
formatter.examples({
  "line": 17,
  "name": "",
  "description": "",
  "id": "automation-assignment-for-goibibo-website;one-way-trip-booking;",
  "rows": [
    {
      "cells": [
        "from",
        "destination",
        "adult",
        "children",
        "infants",
        "travel_class"
      ],
      "line": 18,
      "id": "automation-assignment-for-goibibo-website;one-way-trip-booking;;1"
    },
    {
      "cells": [
        "Pune",
        "Delhi",
        "2",
        "1",
        "1",
        "Economy"
      ],
      "line": 19,
      "id": "automation-assignment-for-goibibo-website;one-way-trip-booking;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 19,
  "name": "One way trip booking",
  "description": "",
  "id": "automation-assignment-for-goibibo-website;one-way-trip-booking;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 6,
  "name": "user is on Goibibo website",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "user click on one way trip button",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "user select \"Pune\" and \"Delhi\" city",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "user select depature_date",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "user select 2 1 1",
  "matchedColumns": [
    2,
    3,
    4
  ],
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "user select \"Economy\"",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "user click on search button",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "click on price ascending",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "click on book now",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "verify details of ticket",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
});