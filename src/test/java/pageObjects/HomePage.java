package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage {
	
	WebDriver driver;
	WebElement signin;
	WebElement register;
	
	
	public HomePage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public void click_sign_in()
	{
		signin=driver.findElement(By.linkText("SIGN IN"));
		signin.click();
	}
	
	public void click_register()
	{
		register=driver.findElement(By.linkText("REGISTER"));
		register.click();
	}

}
