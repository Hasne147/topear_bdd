package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ResetPassPage {
	
	WebDriver driver;
	WebElement pemail_box;
	WebElement create_pass_button;
	
	public ResetPassPage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	
	public void enter_pemail(String email)
	{
		pemail_box=driver.findElement(By.id("phdesktopbody_0_username"));
		pemail_box.sendKeys(email);
	}
	
	public void click_create_pass()
	{
		create_pass_button=driver.findElement(By.name("phdesktopbody_0$Create Your New Password"));
		create_pass_button.click();
	}

}
