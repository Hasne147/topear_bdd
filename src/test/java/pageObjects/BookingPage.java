package pageObjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BookingPage {
	
	WebDriver driver;
	WebElement price_label;
	WebElement book_button;
	
	public BookingPage(WebDriver driver)
	{
		this.driver=driver;
		
		
	}
	
	
	public void assend_price()
	{
		List<String> list=new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(list.get(0));
		price_label=driver.findElement(By.xpath("//*[@id=\"PRICE\"]/span/span"));
		
	}
	
	public void click_book_now()
	{
		List<String> list=new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(list.get(0));
		book_button=driver.findElement(By.cssSelector("input[class='button fr fltbook fb widthF105 fb quicks'][value='BOOK']"));
		book_button.click();
	}

}
