package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class OneWayPage {
	
	WebDriver driver;
	WebElement from_city;
	WebElement destination_city;
	WebElement adult_plus_button;
	WebElement child_plus_button;
	WebElement infant_plus_button;
	WebElement class_textbox;
	WebElement search_button;
	
	public OneWayPage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	
	public void enter_from_city(String from)
	{
		from_city=driver.findElement(By.id("gosuggest_inputSrc"));
		//Select s1=new Select(from_city);
		//s1.selectByVisibleText(from);
		from_city.sendKeys(from);
	}
	
	
	public void enter_destination_city(String destination)
	{
		destination_city=driver.findElement(By.id("gosuggest_inputDest"));
		//Select s1=new Select(destination_city);
		//s1.selectByVisibleText(destination);
		destination_city.sendKeys(destination);
	}
	
    
	public void select_adult(int adult)
	{
		driver.findElement(By.id("pax_link_common")).click();
		adult_plus_button=driver.findElement(By.id("adultPaxPlus"));
		for(int i=0;i<adult-1;i++)
			adult_plus_button.click();
	}
	
	public void select_children(int child)
	{
		//driver.findElement(By.id("pax_link_common")).click();
		child_plus_button=driver.findElement(By.id("childPaxPlus"));
		for(int i=0;i<child;i++)
			child_plus_button.click();
	}
	
	
	public void select_infant(int infant)
	{
		//driver.findElement(By.id("pax_link_common")).click();
		infant_plus_button=driver.findElement(By.id("infantPaxPlus"));
		for(int i=0;i<infant;i++)
			infant_plus_button.click();
	}
	
	
	public void select_class(String class1)
	{
		class_textbox=driver.findElement(By.id("gi_class"));
		Select s2=new Select(class_textbox);
		s2.selectByVisibleText(class1);
	}
	
	
	public void click_search()
	{
		search_button=driver.findElement(By.id("gi_search_btn"));
		search_button.click();
	}
	

}
