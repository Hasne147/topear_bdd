package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class GoibiboPage {
	
	WebDriver driver;
	WebElement one_way_button;
	
	
	public GoibiboPage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public void click_one_way()
	{
		one_way_button=driver.findElement(By.id("oneway"));
		one_way_button.click();
	}

}
