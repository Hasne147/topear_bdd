package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class RegistrationPage {
	
	WebDriver driver;
	WebElement fname_box;
	WebElement lname_box;
	WebElement remail_box;
	WebElement rpassword_box;
	WebElement confirmpassword_box;
	WebElement birth_month_box;
	WebElement birth_year_box;
	WebElement zip_code;
	WebElement create_acc_button;
	
	
	
	public RegistrationPage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public void enter_fname(String fname)
	{
		fname_box=driver.findElement(By.name("phdesktopbody_0$phdesktopbody_0_grs_consumer[firstname]"));
		fname_box.sendKeys(fname);
	}
	
	public void enter_lname(String lname)
	{
		lname_box=driver.findElement(By.name("phdesktopbody_0$phdesktopbody_0_grs_consumer[lastname]"));
		lname_box.sendKeys(lname);
	}
	
	public void enter_remail(String email)
	{
		remail_box=driver.findElement(By.id("phdesktopbody_0_grs_account[emails][0][address]"));
		remail_box.sendKeys(email);
	}
	
	public void enter_rpassword(String pass)
	{
		rpassword_box=driver.findElement(By.id("phdesktopbody_0_grs_account[password][password]"));
		rpassword_box.sendKeys(pass);
	}
	
	public void confirm_password(String pass1)
	{
		confirmpassword_box=driver.findElement(By.id("phdesktopbody_0_grs_account[password][confirm]"));
		confirmpassword_box.sendKeys(pass1);
	}
	
	public void select_month(String month)
	{
		birth_month_box=driver.findElement(By.id("phdesktopbody_0_grs_consumer[birthdate][month]"));
		Select select_1=new Select(birth_month_box);
		select_1.selectByVisibleText(month);
	}
	
	public void select_year(String year)
	{
		birth_year_box=driver.findElement(By.id("phdesktopbody_0_grs_consumer[birthdate][year]"));
		Select select_2=new Select(birth_year_box);
		select_2.selectByVisibleText(year);
	}
	
	public void enter_zip(String code)
	{
		zip_code=driver.findElement(By.name("phdesktopbody_0$phdesktopbody_0_grs_account[addresses][0][postalarea]"));
		zip_code.sendKeys(code);
	}
	
	public void create_acc()
	{
		create_acc_button=driver.findElement(By.name("phdesktopbody_0$phdesktopbody_0_submit"));
		create_acc_button.click();
	}

	


}
