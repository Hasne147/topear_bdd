package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LogInPage {
	
	WebDriver driver;
	WebElement email_box;
	WebElement password_box;
	WebElement login_button;
	WebElement forget_pass;
	
	
	
	public LogInPage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public void enter_email(String email)
	{
		email_box=driver.findElement(By.name("phdesktopbody_0$phdesktopbody_0_username"));
		email_box.sendKeys(email);
	}
	
	public void enter_password(String pass)
	{
		email_box=driver.findElement(By.name("phdesktopbody_0$ctl04"));
		email_box.sendKeys(pass);
	}
	
	public void click_login()
	{
		login_button=driver.findElement(By.id("phdesktopbody_0_Sign In"));
		login_button.click();
	}
	
	
	public void click_forget_pass()
	{
		forget_pass=driver.findElement(By.id("phdesktopbody_0_forgotpassword"));
		forget_pass.click();
	}

}
