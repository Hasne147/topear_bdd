package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		 features = "src/test/java/feature/Assignment_1_feature.feature",
		 glue = {"src/test/java/stepDefination/assignment_1_step.java"},
		 plugin = {"pretty","html:target/cucumber-reports"},
		 monochrome=true
		 )

public class assign_1_runner {

}
