package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		 features = "src/test/java/feature/Assignment_2_feature.feature",
		 glue = {"src/test/java/stepDefination/assignment_2_step.java"},
		 plugin = {"pretty","html:target/cucumber-reports"},
		 monochrome=true
		 )

public class assign_2_runner {

}
