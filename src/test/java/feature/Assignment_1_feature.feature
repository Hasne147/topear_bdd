Feature: Automation assignment for gillette website

Scenario Outline: Registration scenario

Given user is on Home page  
When user click on register button
And user fill the details
|first_name|last_name|email|password|birth_month|birth_year|zip_code|
|<first_name>|<last_name>|<email>|<password>|<birth_month>|<birth_year>|<zip_code>|
And user click on create your profile button
Then verify registration complete text

Examples:
|first_name|last_name|email|password|birth_month|birth_year|zip_code|
|Sneha|Shinde|zxciviv@gmail.com|gillette14|5|1998|416416|

Scenario Outline: Log in scenario
Given user is on Home page
When user click on sign in button
And user enters "<email>" and "<password>"
And user click on log in button
Then verify welcome text

Examples:
|email|password|
|zxcv@gmail.com|gillette14|

Scenario Outline: Forget password and reset password scenario
Given user is on Sign in page
When user click on forget password button
And user enters "<pemail>"
And user click on create new password 
Then verify the sent mail text

Examples:
|pemail|
|zxcv@gmail.com|





