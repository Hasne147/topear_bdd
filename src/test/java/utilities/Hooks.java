package utilities;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {

	
	public static WebDriver driver1;

	
	@Before
	public void set_up_browser()
	{
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium_softwares\\chromedriver_win32\\chromedriver.exe");
		driver1=new ChromeDriver();
		driver1.manage().window().maximize();
		driver1.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver1.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
	}
	
	@After
	public void tear_down_browser()
	{
		driver1.close();
		driver1.quit();
	}



}
