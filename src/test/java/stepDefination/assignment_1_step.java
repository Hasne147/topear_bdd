package stepDefination;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import pageObjects.HomePage;
import pageObjects.LogInPage;
import pageObjects.RegistrationPage;
import pageObjects.ResetPassPage;

public class assignment_1_step {
	
   WebDriver driver2;
   HomePage hpage;
   RegistrationPage rpage;
   LogInPage lpage;
   ResetPassPage rp;
   
   //Registration scenario
	@Given("^user is on Home page$")
	public void user_is_on_Home_page() throws Throwable {
		
		driver2=utilities.Hooks.driver1;
		driver2.get("https://www.gillette.co.in/en-in"); 
	     Thread.sleep(3000);
	}

	@When("^user click on register button$")
	public void user_click_on_register_button() throws Throwable {
		
		driver2=utilities.Hooks.driver1;
		hpage=new HomePage(driver2);
		hpage.click_register();
	     Thread.sleep(3000);
	   
	   
	}

	@When("^user fill the details$")
	public void user_fill_the_details(DataTable arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
	    // E,K,V must be a scalar (String, Integer, Date, enum etc)
		
		driver2=utilities.Hooks.driver1;
		rpage=new RegistrationPage(driver2);
		
		List<Map<String,String>> list=arg1.asMaps(String.class, String.class);
		
		rpage.enter_fname(list.get(0).get("first_name"));
		rpage.enter_lname(list.get(0).get("last_name"));
		rpage.enter_remail(list.get(0).get("email"));
	     Thread.sleep(3000);
		rpage.enter_rpassword(list.get(0).get("password"));
		rpage.confirm_password(list.get(0).get("password"));
	     Thread.sleep(3000);
		rpage.select_month(list.get(0).get("birth_month"));
		rpage.select_year(list.get(0).get("birth_year"));
		rpage.enter_zip(list.get(0).get("zip_code"));
	     Thread.sleep(3000);
	    
	}

	@When("^user click on create your profile button$")
	public void user_click_on_create_your_profile_button() throws Throwable {
		driver2=utilities.Hooks.driver1;
		rpage=new RegistrationPage(driver2);
		
		rpage.create_acc();
	     Thread.sleep(3000);
	    
	}

	@Then("^verify registration complete text$")
	public void verify_registration_complete_text() throws Throwable {
		driver2=utilities.Hooks.driver1;
		String actual=driver2.findElement(By.id("phdesktopbody_0_Header")).getText();
		String expected="YOUR REGISTRATION IS COMPLETE";
		Assert.assertEquals(expected, actual);
	    
	    
	}
	
	//Log in scenario

	@When("^user click on sign in button$")
	public void user_click_on_sign_in_button() throws Throwable {
		
		driver2=utilities.Hooks.driver1;
		hpage=new HomePage(driver2);
		hpage.click_sign_in();
	     Thread.sleep(3000);
	   
	    
	}

	@When("^user enters \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_enters_and(String arg1, String arg2) throws Throwable {
	    
		driver2=utilities.Hooks.driver1;
		lpage=new LogInPage(driver2);
		
		lpage.enter_email(arg1);
		lpage.enter_password(arg2);
		Thread.sleep(3000);
		
		
		
	}
	
	@When("^user click on log in button$")
	public void user_click_on_log_in_button() throws Throwable
	{
		driver2=utilities.Hooks.driver1;
		lpage=new LogInPage(driver2);
		
		lpage.click_login();
		Thread.sleep(3000);
	}

	@Then("^verify welcome text$")
	public void verify_welcome_text() throws Throwable {
	   
		driver2=utilities.Hooks.driver1;
		String actual=driver2.findElement(By.xpath("//*[@id=\"phdesktopheader_0_phdesktopheadertop_2_pnlLogOffLink\"]/div/span")).getText();
		String expected="Welcome,";
		Assert.assertEquals(expected, actual);
	    
	}

	@Given("^user is on Sign in page$")
	public void user_is_on_Sign_in_page() throws Throwable {
	    
		driver2=utilities.Hooks.driver1;
		
		driver2.get("https://www.gillette.co.in/en-in/loginpage");
		
	}

	@When("^user click on forget password button$")
	public void user_click_on_forget_password_button() throws Throwable {
		driver2=utilities.Hooks.driver1;
		lpage=new LogInPage(driver2);
		
		lpage.click_forget_pass();
	   
	}

	@When("^user enters \"([^\"]*)\"$")
	public void user_enters(String arg1) throws Throwable {
	   
		driver2=utilities.Hooks.driver1;
		rp=new ResetPassPage(driver2);
	   
		rp.enter_pemail(arg1);
	}

	@When("^user click on create new password$")
	public void user_click_on_create_new_password() throws Throwable {
		
		driver2=utilities.Hooks.driver1;
		rp=new ResetPassPage(driver2);
		
		rp.click_create_pass();
	   
	}

	@Then("^verify the sent mail text$")
	public void verify_the_sent_mail_text() throws Throwable {
	    
		driver2=utilities.Hooks.driver1;
		String actual=driver2.findElement(By.xpath("//*[@id=\"phdesktopbody_0_afterSubmit\"]/div[2]/h2")).getText();
		System.out.println(actual);
		String expected="You will receive an e-mail very shortly containing a link to reset your password.";
		Assert.assertEquals(expected, actual);
	}



}
