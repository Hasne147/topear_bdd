package stepDefination;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import pageObjects.BookingPage;
import pageObjects.GoibiboPage;
import pageObjects.OneWayPage;

public class assignment_2_step {
	
	WebDriver driver2;
	GoibiboPage gp;
	OneWayPage ow;
	BookingPage bpage;
	
	@Given("^user is on Goibibo website$")
	public void user_is_on_Goibibo_website() throws Throwable {
		
		driver2=utilities.Hooks.driver1;
		driver2.get("https://www.goibibo.com/");
		Thread.sleep(3000);
	    
	}

	@When("^user click on one way trip button$")
	public void user_click_on_one_way_trip_button() throws Throwable {
	  
		driver2=utilities.Hooks.driver1;
		gp=new GoibiboPage(driver2);
		
		gp.click_one_way();
		Thread.sleep(3000);
	}

	@When("^user select \"([^\"]*)\" and \"([^\"]*)\" city$")
	public void user_select_and_city(String arg1, String arg2) throws Throwable {
		
		driver2=utilities.Hooks.driver1;
		ow=new OneWayPage(driver2);
		
		ow.enter_from_city(arg1);
		ow.enter_destination_city(arg2);
		Thread.sleep(3000);
		
	    
	}

	@When("^user select depature_date$")
	public void user_select_depature_date() throws Throwable {
		
		driver2=utilities.Hooks.driver1;
		driver2.findElement(By.id("departureCalendar")).click();
		driver2.findElement(By.id("fare_20200730")).click();
		Thread.sleep(3000);
	    
	}

	@When("^user select (\\d+) (\\d+) (\\d+)$")
	public void user_select(int arg1, int arg2, int arg3) throws Throwable {
		
		driver2=utilities.Hooks.driver1;
		ow=new OneWayPage(driver2);
		
		ow.select_adult(arg1);
		Thread.sleep(3000);
		ow.select_children(arg2);
		Thread.sleep(3000);
		ow.select_infant(arg3);
		Thread.sleep(3000);
	    
	}

	@When("^user select \"([^\"]*)\"$")
	public void user_select(String arg1) throws Throwable {
		
		driver2=utilities.Hooks.driver1;
		ow=new OneWayPage(driver2);
		
		ow.select_class(arg1);
		Thread.sleep(3000);
	    
	}

	@When("^user click on search button$")
	public void user_click_on_search_button() throws Throwable {
		
		driver2=utilities.Hooks.driver1;
		ow=new OneWayPage(driver2);
		
		ow.click_search();
		Thread.sleep(4000);
	    
	}

	@When("^click on price ascending$")
	public void click_on_price_ascending() throws Throwable {
		
		driver2=utilities.Hooks.driver1;
		bpage=new BookingPage(driver2);
		
		bpage.assend_price();
		Thread.sleep(3000);
	    
	}

	@When("^click on book now$")
	public void click_on_book_now() throws Throwable {
		
		driver2=utilities.Hooks.driver1;
		bpage=new BookingPage(driver2);
		
		bpage.click_book_now();
		Thread.sleep(3000);
	    
	}

	@Then("^verify details of ticket$")
	public void verify_details_of_ticket() throws Throwable {
		
		driver2=utilities.Hooks.driver1;
		String actual=driver2.findElement(By.className("fl padLR10 padT5 ico18 quicks fb")).getText();
		String expected="Ticket Details";
		Assert.assertEquals(expected, actual);
		
	    
	}
}
